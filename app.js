versionNav();
tamañoPantalla();
concatenarDias();

function transformarHora(){
    const date = new Date();
    const hour = date.getHours();
    const minutes = date.getMinutes();
    const seg = date.getSeconds();

    document.getElementById('Hour').innerHTML = `${hour*3600};${minutes*60};${seg} seg`; 

    console.log(hour*3600);
}

function calcArea(){
    const base = document.getElementById('base').value;
    const altura = document.getElementById('altura').value;

    document.getElementById('valor-area').innerHTML = (base*altura)/2;
}

function mostrarRaiz(){
    const numero = document.getElementById('num-impar').value;
    let raiz;       //VARIABLE PARA GUARDAR LA RAIZ ENCONTRADA
    
    for(let num = 0; num<numero; num++){       //CICLO PARA CONOCER CUAL ES EL NUMERO QUE MAS SE APROXIMA A LA RAIZ
        if(num*num>numero){         //SI EL NUMERO MULTIPLICADO POR EL MISMO ES IGUAL A LA RAIZ
            const minimo = num-1;  
            let ant = 0;        //ACUMULADOR DE DECIMAL

            for(let i=0; i<99; i++){       //ENCAPSULA TODAS LAS DECIMALES CON MAXIMO DE .99
                ant = ant + .01;
                const numAct = Number((minimo+ant).toFixed(2));
                
                if(numAct*numAct >= numero){
                    raiz = numAct;
                    document.getElementById('value-raiz').innerHTML = raiz;
                    return;
                }
            }
            return;
        }else if(num*num == numero){
            raiz = num;
            document.getElementById('value-raiz').innerHTML = raiz;
            return;
        }
    }    
}

function mostrarLong(){    
    let resultado = document.getElementById('valor-length');
    const longitud = document.getElementById('textoLong').value.length;

    if(resultado.innerText == ''){
        resultado.innerHTML = longitud;
    }else{
        resultado.innerHTML = '';
        resultado.innerHTML = longitud;
    }
}

function concatenarDias(){
    const arra1 = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes'];
    const arra2 = ['Sábado', 'Domingo'];
    let concat = arra1+arra2;

    document.getElementById('a1').innerHTML = arra1;
    document.getElementById('a2').innerHTML = arra2;
    document.getElementById('diasConcat').innerHTML = concat;
}

function versionNav(){
    document.getElementById('version').innerHTML = `${navigator.platform} , ${navigator.appCodeName} , ${navigator.appName}`
}

function tamañoPantalla(){
    const height = screen.height;
    const width = screen.width;
    document.getElementById('pantHeight').innerHTML = height;
    document.getElementById('pantWidth').innerHTML = width;
}

function printDisplay(){
    window.print();
}

